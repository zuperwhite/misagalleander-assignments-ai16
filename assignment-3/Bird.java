/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssample;

/**
 *
 * @author laboratory
 */
abstract public class Bird {
    protected FlySuperClass flyAbility;
    protected SwimSuperClass swimAbility;
    protected String name;
    Bird(FlySuperClass fly, SwimSuperClass swims, String name){
        this.flyAbility = fly;
        this.swimAbility = swims;
        this.name = name;
    }
    public void fly(){
        this.flyAbility.fly();
        System.out.print(this.name);
    }
    public void swim(){
        this.swimAbility.swim();
        System.out.print(this.name);
    }
}
